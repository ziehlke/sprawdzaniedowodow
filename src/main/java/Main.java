import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj numer i serie dowodu osobistego: ");

//        String input = scanner.nextLine();
        String input = "CAA348124";

        char[] litery = input.substring(0, 3).toUpperCase().toCharArray();
        char[] cyfry = input.substring(3).toCharArray();
        List<Integer> doSprawdzenia = new ArrayList<>();
        int[] multipliers = {7, 3, 1, 9, 7, 3, 1, 7, 3};

//        System.out.println(litery);
//        System.out.println(cyfry);

        for (char c : litery) {
            doSprawdzenia.add((int) c - 55);
        }

        for (char c : cyfry) {
            doSprawdzenia.add((int) c - 48);
        }

        System.out.println(doSprawdzenia);


        int sum = 0;
        for (int i = 0; i < 9; i++) {
            sum += doSprawdzenia.get(i) * multipliers[i];
//            System.out.print(doSprawdzenia.get(i) + " * " + (multipliers[i]) + " + ");
        }

        System.out.println();

        if (sum % 10 == 0) {
            System.out.println("podane dane sa prawidlowe.");
        } else {
            System.out.println("podales nieprawdziwe dane.");
        }

    }
}
